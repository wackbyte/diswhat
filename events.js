import fs from "fs";
import path from "path";

const EVENTS = [
	"join_voice_channel",
	"start_speaking",
	"app_opened",
	"add_reaction",
	"guild_joined",
	"leave_guild",
	"message_edited",
	"message_deleted",
	"channel_opened",
	"captcha_solved",
];
const eventCount = {};

EVENTS.forEach(e => {
	eventCount[e] = 0;
});

// Okay, this is really funny/stupid. I'll share it. I like putting anecdotes in my code
// I wrote all of this, and it worked like a charm. Then, I got my new data package (6+ yrs)
// and tried to load it, but node... crashed???
// `Error: Cannot create a string longer than 0x1fffffe8 characters`
// TODAY I LEARNED
// Pipes or streams or whatever superiority

export function getEventData(BASE) {
	return new Promise((resolve, reject) => {
		// let shit = fs
		// 	.readFileSync()
		// 	.toString()
		// 	.trim()
		// 	.split("\n")
		// 	.map(line => JSON.parse(line));

		let find2 = fs.readdirSync(path.join(BASE, "activity", "tns"))[0];
		// Idk why but it only works this way. I can't concat the arrays
		let shit2 = fs
			.readFileSync(path.join(BASE, "activity", "tns", find2))
			.toString()
			.trim()
			.split("\n")
			.map(line => JSON.parse(line));

		// shit.forEach(e => {
		// 	// if (!allE.includes(e.event_type)) allE.push(e.event_type);
		//
		// });
		shit2.forEach(e => {
			// if (!allE.includes(e.event_type)) allE.push(e.event_type);
			if (EVENTS.includes(e.event_type)) eventCount[e.event_type]++;
		});

		// console.log(allE);

		let find = fs.readdirSync(path.join(BASE, "activity", "reporting"))[0];

		let stream = fs.createReadStream(
			path.join(BASE, "activity", "reporting", find)
		);
		let incompleteBit = "";
		stream.on("data", c => {
			c = c.toString();
			c = c.split("\n");
			incompleteBit += c.shift();
			let j = JSON.parse(incompleteBit);
			if (EVENTS.includes(j.event_type)) eventCount[j.event_type]++;

			let stop = c.length - 1;
			c.forEach((e, i) => {
				if (i === stop) return;
				e = JSON.parse(e);
				if (EVENTS.includes(e.event_type)) eventCount[e.event_type]++;
			});

			incompleteBit = c[c.length - 1];
		});

		stream.on("end", () => {
			resolve(eventCount);
		});
	});
}
