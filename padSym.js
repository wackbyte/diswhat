export function pad(str) {
	return str.padStart(2).padEnd(3);
}

export function ct(sym, num, thresh2, thresh3) {
	return pad(sym.repeat(num > thresh3 ? 3 : num > thresh2 ? 2 : 1));
}

export function oneOf(list, num, fallback) {
	let out = fallback;
	list.forEach(el => {
		if (num >= el[1] && out === fallback) out = el[0];
	});
	return pad(out);
}
